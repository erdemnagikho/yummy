<?php
require_once("includes/config.php");
require_once("includes/classes/FormSanitizer.php");
require_once("includes/classes/Constants.php");
require_once("includes/classes/Account.php");
require_once("includes/classes/Helper.php");

$account = new Account($connection); // connection comes from config.php

if (isset($_POST['submitButton'])) {
    $firstName = FormSanitizer::sanitizeFormString($_POST['firstName']);
    $lastName = FormSanitizer::sanitizeFormString($_POST['lastName']);
    $username = FormSanitizer::sanitizeFormUsername($_POST['username']);
    $email = FormSanitizer::sanitizeFormEmail($_POST['email']);
    $password = FormSanitizer::sanitizeFormPassword($_POST['password']);
    $password2 = FormSanitizer::sanitizeFormPassword($_POST['password2']);

    $success = $account->register($firstName, $lastName, $username, $email, $password, $password2);

    if ($success) {
        header("Location: login.php");
    }
}
?>

<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kayıt Ol!</title>
    <link rel="stylesheet" type="text/css" href="assets/style/style.css">
</head>
<body>
<div class="signInContainer">
    <div class="column">
        <div class="header">
            <img src="assets/images/yummyflix.png" alt="Site Logo" title="Logo">
            <h3>Kayıt Ol</h3>
        </div>
        <form method="post">
            <?php echo $account->getError(Constants::$firstNameCharacters); ?>
            <input type="text" name="firstName" placeholder="Ad" value="<?php Helper::getInputValue('firstName'); ?>"
                   required>

            <?php echo $account->getError(Constants::$lastNameCharacters); ?>
            <input type="text" name="lastName" placeholder="Soyad" value="<?php Helper::getInputValue('lastName'); ?>"
                   required>

            <?php echo $account->getError(Constants::$usernameCharacters); ?>
            <?php echo $account->getError(Constants::$usernameTaken); ?>
            <input type="text" name="username" placeholder="Kullanıcı Adı"
                   value="<?php Helper::getInputValue('username'); ?>" required>

            <?php echo $account->getError(Constants::$emailInvalid); ?>
            <?php echo $account->getError(Constants::$emailTaken); ?>
            <input type="text" name="email" placeholder="E-Posta" value="<?php Helper::getInputValue('email'); ?>"
                   required>

            <?php echo $account->getError(Constants::$passwordsDoNotMatch); ?>
            <?php echo $account->getError(Constants::$passwordLength); ?>
            <input type="password" name="password" placeholder="Şifre" required>
            <input type="password" name="password2" placeholder="Şifre Doğrula" required>

            <input type="submit" name="submitButton" value="Kayıt Ol">
        </form>
        <a href="login.php" class="signInMessage">Hesabınız var ise, buradan giriş yapın</a>
    </div>
</div>
</body>
</html>