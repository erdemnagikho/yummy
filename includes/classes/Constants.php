<?php

class Constants {
    public static $firstNameCharacters = "İsminiz en az 2, en fazla 25 karakterden oluşmalıdır.";
    public static $lastNameCharacters = "Soyadınız en az 2, en fazla 25 karakterden oluşmalıdır.";
    public static $usernameCharacters = "Kullanıcı adınız en az 2, en fazla 25 karakterden oluşmalıdır.";
    public static $usernameTaken = "Bu kullanıcı adı ile daha önce kayıt olunmuş.";
    public static $emailInvalid = "Geçersiz e-posta adresi.";
    public static $emailTaken = "Bu e-posta ile daha önce kayıt olunmuş.";
    public static $passwordsDoNotMatch = "Şifreler eşleşmiyor.";
    public static $passwordLength = "Şifreniz en az 2, en fazla 25 karakterden oluşmalıdır.";
    public static $loginFailed = "Kullanıcı adınız veya şifreniz yanlış.";
}
