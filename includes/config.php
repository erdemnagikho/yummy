<?php
ob_start(); // Turns on output buffering
session_start();

date_default_timezone_set("Europe/Istanbul");

try {
    $connection = new PDO("mysql:dbname=food_flix;host=localhost", "root", "");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
} catch (PDOException $e) {
    exit("Veritabanı bağlantı sorunu " . $e->getMessage());
}