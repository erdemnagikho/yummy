<?php
require_once("includes/header.php");

$preview = new PreviewProvider($connection, $userLoggedIn);
$preview->createPreviewVideo(null);

$categoryContainers = new CategoryContainers($connection, $userLoggedIn);
$categoryContainers->showAllCategories();