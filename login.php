<?php
require_once("includes/config.php");
require_once("includes/classes/FormSanitizer.php");
require_once("includes/classes/Constants.php");
require_once("includes/classes/Account.php");
require_once("includes/classes/Helper.php");

$account = new Account($connection); // connection comes from config.php

if (isset($_POST['submitButton'])) {
    $username = FormSanitizer::sanitizeFormUsername($_POST['username']);
    $password = FormSanitizer::sanitizeFormPassword($_POST['password']);

    $success = $account->login($username, $password);

    if ($success) {
        $_SESSION["userLoggedIn"] = $username;
        header("Location: index.php");
    }
}
?>

<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kayıt Ol!</title>
    <link rel="stylesheet" type="text/css" href="assets/style/style.css">
</head>
<body>
<div class="signInContainer">
    <div class="column">
        <div class="header">
            <img src="assets/images/yummyflix.png" alt="Site Logo" title="Logo">
            <h3>Giriş Yap</h3>
        </div>
        <form method="post">
            <?php echo $account->getError(Constants::$loginFailed); ?>
            <input type="text" name="username" placeholder="Kullanıcı Adı"
                   value="<?php Helper::getInputValue('username'); ?>"
                   required>

            <input type="password" name="password" placeholder="Şifre" required>
            <input type="submit" name="submitButton" value="Giriş Yap">
        </form>
        <a href="register.php" class="signInMessage">Hesabınız yok ise, buradan kayıt olun</a>
    </div>
</div>
</body>
</html>