<?php
require_once("admin_header.php");

$entityProvider = new AdminEntityProvider();

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$limit = 10;
$startFrom = ($page - 1) * $limit;
$entities = $entityProvider->getEntitiesWithPaginate($connection, $startFrom, $limit);

$totalPage = $entityProvider->getEntitiesTotalPage($connection);
?>

<div class="container mt-3">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Thumbnail</th>
            <th scope="col">Preview</th>
            <th scope="col">Category</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($entities as $entity) {
            echo "<tr>
                    <th>".$entity['id']."</th>
                    <th>".$entity['name']."</th>
                    <th><img src=".'http://localhost/foodflix/'.$entity['thumbnail']." height='130' width='200'></th>
                    <th>".$entity['preview']."</th>
                    <th>".$entity['category']."</th>
                  </tr>";
        }
        ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php for ($i = 1; $i <= $totalPage; $i++) { ?>
                <li class="page-item <?php if ($i == $page) { echo "active"; } ?>"><a class="page-link" href="entities.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php } ?>
        </ul>
    </nav>
</div>
