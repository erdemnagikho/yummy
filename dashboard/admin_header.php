<?php
require_once("../includes/config.php");
require_once("../dashboard/classes/AdminCategoryProvider.php");
require_once("../dashboard/classes/AdminEntityProvider.php");
require_once("../dashboard/classes/AdminVideoProvider.php");

if (!isset($_SESSION["adminLoggedIn"])) {
    header("Location: login.php");
}

$adminLoggedIn = $_SESSION["adminLoggedIn"];

$activePage = basename($_SERVER['PHP_SELF']);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DASHBOARD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/06a651c8da.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="assets/js/script.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><?php echo $adminLoggedIn; ?></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?php if ($activePage == 'categories.php') {
                        echo 'active';
                    } ?>" href="categories.php">Categories</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($activePage == 'entities.php') {
                        echo 'active';
                    } ?>" href="entities.php">Entities</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($activePage == 'videos.php') {
                        echo 'active';
                    } ?>" href="videos.php">Videos</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

