<?php
require_once("admin_header.php");

if (isset($_GET['id'])) {
    $categoryId = $_GET['id'];
} else {
    die("No category id");
}

$category = AdminCategoryProvider::getCategoryById($connection, $categoryId);

$url = htmlspecialchars($_SERVER["PHP_SELF"]) . "?id=" . $categoryId;

if (is_null($category)) {
    die("No category");
}

$message = "";
$class = "";

if (isset($_POST['submitButton'])) {
    $categoryName = AdminCategoryProvider::sanitizeFormCategoryName($_POST['name']);

    if ($categoryName != null || $categoryName != "") {
        $success = AdminCategoryProvider::updateCategory($connection, $category, $categoryName);

        if ($success) {
            $message = "Category updated successfully";
            $class = "text-success";

            //header("Location: " . $url);
        } else {
            die("Category could not update");
        }
    }
}

?>

<div class="container mt-3">
    <div class="row">
        <div class="col-md-6 offset-3">
            <form method="post" action="<?php echo $url; ?>">
                <div class="form-group">
                    <label for="formGroupExampleInput" class="form-label">Category Name</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $category['name']; ?>"
                           required>
                    <span class="<?php echo $class; ?>"><?php echo $message; ?></span>
                </div>
                <div class="form-group mt-3">
                    <input class="btn btn-warning" type="submit" name="submitButton" value="Edit" required>
                </div>
            </form>
        </div>
    </div>
</div>
