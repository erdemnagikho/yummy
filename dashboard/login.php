<?php
require_once("../includes/config.php");
require_once("../includes/classes/FormSanitizer.php");
require_once("../includes/classes/Constants.php");
require_once("../includes/classes/Account.php");
require_once("../includes/classes/Helper.php");

$account = new Account($connection); // connection comes from config.php

if (isset($_POST['submitButton'])) {
    $email = FormSanitizer::sanitizeFormEmail($_POST['email']);
    $password = FormSanitizer::sanitizeFormPassword($_POST['password']);

    $success = $account->managerLogin($email, $password);

    if ($success) {
        $_SESSION["adminLoggedIn"] = $email;
        header("Location: index.php");
    }
}
?>

<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Giriş</title>
    <link rel="stylesheet" type="text/css" href="../assets/style/style.css">
</head>
<body>
<div class="signInContainer">
    <div class="column">
        <div class="header">
            <img src="../assets/images/yummyflix.png" alt="Site Logo" title="Logo">
            <h3>Admin</h3>
        </div>
        <form method="post">
            <?php echo $account->getError(Constants::$loginFailed); ?>
            <input type="text" name="email" placeholder="E-Posta"
                   value="<?php Helper::getInputValue('email'); ?>"
                   required>

            <input type="password" name="password" placeholder="Şifre" required>
            <input type="submit" name="submitButton" value="Giriş Yap">
        </form>
    </div>
</div>
</body>
</html>