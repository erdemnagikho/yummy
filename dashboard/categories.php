<?php
require_once("admin_header.php");

$categoryProvider = new AdminCategoryProvider();

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$limit = 10;
$startFrom = ($page - 1) * $limit;
$categories = $categoryProvider->getCategoriesWithPaginate($connection, $startFrom, $limit);

$totalPage = $categoryProvider->getCategoriesTotalPage($connection);
?>

<div class="container mt-3">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Settings</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categories as $category) { ?>
            <tr>
                <th><?php echo $category['id']; ?></th>
                <th><?php echo $category['name']; ?></th>
                <th><a class="btn btn-warning" href="category.php?id=<?php echo $category['id']; ?>">Edit</a></th>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
             <?php for ($i = 1; $i <= $totalPage; $i++) { ?>
                 <li class="page-item <?php if ($i == $page) { echo "active"; } ?>"><a class="page-link" href="categories.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
             <?php } ?>
        </ul>
    </nav>
</div>
