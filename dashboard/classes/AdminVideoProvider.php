<?php


class AdminVideoProvider
{
    public static function getVideosWithPaginate($connection, $startFrom, $limit = 10)
    {
        try {
            $sql = "SELECT v.*, e.name as entity FROM videos v INNER JOIN entities e ON e.id = v.entityId ";
            $sql .= "LIMIT :startFrom, :limit";

            $query = $connection->prepare($sql);

            $query->bindValue(":startFrom", $startFrom, PDO::PARAM_INT);
            $query->bindValue(":limit", $limit, PDO::PARAM_INT);

            $query->execute();

            $result = array();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $result[] = [
                    'id' => $row['id'],
                    'title' => $row['title'],
                    'description' => $row['description'],
                    'filePath' => $row['filePath'],
                    'isMovie' => $row['isMovie'],
                    'uploadDate' => $row['uploadDate'],
                    'releaseDate' => $row['releaseDate'],
                    'views' => $row['views'],
                    'duration' => $row['duration'],
                    'season' => $row['season'],
                    'episode' => $row['episode'],
                    'entity' => $row['entity'],
                ];
            }

            return $result;
        } catch (Exception $e) {
            echo '<p>', $e->getMessage(), '</p>';
        }

        return null;
    }

    public static function getVideosTotalPage($connection, $limit = 10)
    {
        $sql = "SELECT COUNT(*) AS totalCount FROM videos";

        $query = $connection->prepare($sql);

        $query->execute();

        $totalData = $query->fetch(PDO::FETCH_COLUMN);

        return ceil($totalData / $limit);;
    }
}