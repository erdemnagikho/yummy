<?php


class AdminCategoryProvider
{
    public static function getCategoriesWithPaginate($connection, $startFrom, $limit = 10)
    {
        try {
            $sql = "SELECT * FROM categories ";
            $sql .= "LIMIT :startFrom, :limit";

            $query = $connection->prepare($sql);

            $query->bindValue(":startFrom", $startFrom, PDO::PARAM_INT);
            $query->bindValue(":limit", $limit, PDO::PARAM_INT);

            $query->execute();

            $result = array();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $result[] = [
                    'id' => $row['id'],
                    'name' => $row['name'],
                ];
            }

            return $result;
        } catch (Exception $e) {
            echo '<p>', $e->getMessage(), '</p>';
        }

        return null;
    }

    public static function getCategoriesTotalPage($connection, $limit = 10)
    {
        $sql = "SELECT COUNT(*) AS totalCount FROM categories";

        $query = $connection->prepare($sql);

        $query->execute();

        $totalData = $query->fetch(PDO::FETCH_COLUMN);

        return ceil($totalData / $limit);
    }

    public static function getCategoryById($connection, $categoryId)
    {
        $sql = "SELECT * FROM categories c WHERE c.id=:categoryId";

        $query = $connection->prepare($sql);

        $query->bindValue(":categoryId", $categoryId, PDO::PARAM_INT);

        $query->execute();

        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public static function sanitizeFormCategoryName($categoryName)
    {
        $categoryName = strip_tags($categoryName);
        $categoryName = str_replace(" ", "", $categoryName);
        return $categoryName;
    }

    public static function updateCategory($connection, $categoryId, $categoryName)
    {
        try {
            $sql = "UPDATE categories SET name=:name WHERE id=:categoryId";

            $query = $connection->prepare($sql);

            $query->bindValue(":categoryId", $categoryId, PDO::PARAM_INT);
            $query->bindValue(":name", $categoryName);

            $result = $query->execute();

            if ($result > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo '<p>', $e->getMessage(), '</p>';
        }

        return false;
    }
}