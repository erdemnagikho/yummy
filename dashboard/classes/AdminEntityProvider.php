<?php


class AdminEntityProvider
{
    public static function getEntitiesWithPaginate($connection, $startFrom, $limit = 10)
    {
        try {
            $sql = "SELECT e.id, e.name, e.thumbnail, e.preview, c.name AS category FROM entities e INNER JOIN categories c ON c.id = e.categoryId ";
            $sql .= "LIMIT :startFrom, :limit";

            $query = $connection->prepare($sql);

            $query->bindValue(":startFrom", $startFrom, PDO::PARAM_INT);
            $query->bindValue(":limit", $limit, PDO::PARAM_INT);

            $query->execute();

            $result = array();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $result[] = [
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'thumbnail' => $row['thumbnail'],
                    'preview' => $row['preview'],
                    'category' => $row['category'],
                ];
            }

            return $result;
        } catch (Exception $e) {
            echo '<p>', $e->getMessage(), '</p>';
        }

        return null;
    }

    public static function getEntitiesTotalPage($connection, $limit = 10)
    {
        $sql = "SELECT COUNT(*) AS totalCount FROM entities";

        $query = $connection->prepare($sql);

        $query->execute();

        $totalData = $query->fetch(PDO::FETCH_COLUMN);

        return ceil($totalData / $limit);;
    }
}