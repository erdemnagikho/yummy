<?php
require_once("admin_header.php");

$videoProvider = new AdminVideoProvider();

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$limit = 10;
$startFrom = ($page - 1) * $limit;
$videos = $videoProvider->getVideosWithPaginate($connection, $startFrom, $limit);

$totalPage = $videoProvider->getVideosTotalPage($connection);
?>

<div class="container mt-3">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">File Path</th>
            <th scope="col">Is Movie</th>
            <th scope="col">Upload Date</th>
            <th scope="col">Release Date</th>
            <th scope="col">Views</th>
            <th scope="col">Duration</th>
            <th scope="col">Season</th>
            <th scope="col">Episode</th>
            <th scope="col">Entity Id</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($videos as $video) {
            echo "<tr>
                    <th>".$video['id']."</th>
                    <th>".$video['title']."</th>
                    <th>".$video['description']."</th>
                    <th>".$video['filePath']."</th>
                    <th>".$video['isMovie']."</th>
                    <th>".$video['uploadDate']."</th>
                    <th>".$video['releaseDate']."</th>
                    <th>".$video['views']."</th>
                    <th>".$video['duration']."</th>
                    <th>".$video['season']."</th>
                    <th>".$video['episode']."</th>
                    <th>".$video['entity']."</th>
                  </tr>";
        }
        ?>
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php for ($i = 1; $i <= $totalPage; $i++) { ?>
                <li class="page-item <?php if ($i == $page) { echo "active"; } ?>"><a class="page-link" href="videos.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php } ?>
        </ul>
    </nav>
</div>
